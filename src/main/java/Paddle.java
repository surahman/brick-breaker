            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Paddle.java                     #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                  Paddle Class.                    #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.util.concurrent.Semaphore;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Paddle extends RoundRectangle2D.Double {

    /* Paddle:
     *  Constructor for the paddle.
     * -Parameters:  double x, double y, double width, double height, double arcDim, int KBStepSize
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Paddle(double x, double y, double width, double height, double arcDim, int KBStepSize)
    {
        // Setup location.
        this.x = x;
        this.y = y;
        this.k_VerticalOffset = width;

        // Set the height, width and chamfer on the edges.
        this.height = height;
        this.width = width;
        this.archeight = this.arcwidth = arcDim;

        // Setup ball colours.
        m_Colour = Color.LIGHT_GRAY;
        m_PwrColour = new Color(255, 102,0);

        // Setup step size of keystroke movements.
        m_KBStepSize = KBStepSize;

        // Setup cheat mutex.
        lk_CheatMutex = new Semaphore(1);
    }



    /* movePaddle:
     *  Move paddles top right X-Coordinate to specified location.
     * -Parameters:  double xCoord, int rightScreenEdge, boolean keystroke
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void movePaddle(double xCoord, int rightScreenEdge, boolean keystroke)
    {

        // Variable Declaration(s):
        double left = 0;                        // Left hand side coordinate of paddle.
        double right = 0;                       // Right hand side coordinate of paddle.


        // Calculate edges points of bar depending on input type.
        if (keystroke)
        {
            left = xCoord;
            right = xCoord + this.width;
        }
        else
        {
            double halfWidth = this.width / 2;
            left = xCoord - halfWidth;
            right = xCoord + halfWidth;
        }


        // Check for bounds and move.
        if (left >= 0 && right <= rightScreenEdge)
        {
            // Set new position.
            x = left;
        }
    }



    /* setVerticalPos:
     *  Adjust vertical position of paddle based on window height.
     * -Parameters:  double windowHeight
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setVerticalPos(double windowHeight)
    {
        this.y = windowHeight - this.k_VerticalOffset;
    }



    /* setWidth:
     *  Set width of paddle.
     * -Parameters:  int width
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setWidth(int width)
    {
        this.width = width;
    }



    /* setHorizontalPos:
     *  Set the horizontal position of the paddle.
     * -Parameters:  double x
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setHorizontalPos(double x)
    {
        this.x = x;
    }



    /* getColour:
     *  Return colour of ball.
     * -Parameters:  n/a
     * -Returns:     Color
     * -Throws:      n/a
     */
    public Color getColour()
    {
        return m_Colour;
    }



    /* cheat:
     *  Activates cheat mode on paddle for <duration> seconds.
     * -Parameters:  int duration
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void cheat(int duration)
    {

        // See if mutex is available otherwise exit.
        if (lk_CheatMutex.availablePermits() == 1)
        {

            // Create cheat thread to execute.
            Thread cheatThread = new Thread()
            {

                @Override
                public void run() {

                    System.err.println("POWER UP: Double <Paddle> width for 10secs.");

                    Color dfault = m_Colour;

                    try
                    {
                        lk_CheatMutex.acquire();
                        System.err.println("CHEAT STARTED");

                        // Double width of <Paddle> and set colour to orange.
                        m_Colour = m_PwrColour;
                        width *= 2;

                        sleep(1000 * duration);
                    }
                    catch (InterruptedException e)
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        // Reset colour and width of <Paddle>.
                        m_Colour = dfault;
                        width /= 2;

                        lk_CheatMutex.release();
                        System.err.println("CHEAT ENDED");
                    }
                }

            };

            cheatThread.start();
        }

    }



    //-- Data Member(s):
    private Color m_Colour;                     // Default <Paddle> colour.
    private Color m_PwrColour;                  // Power-up <Paddle> colour.
    public int m_KBStepSize = 0;                // Step size for keyboard movement.
    private Semaphore lk_CheatMutex;            // Mutex to control access to cheat.
    private final double k_VerticalOffset;      // Offset from bottom of display.
}
