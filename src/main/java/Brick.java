            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Brick.java                      #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                   Brick Class.                    #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.geom.Rectangle2D;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Brick extends Rectangle2D.Double {

    /* Brick:
     *  Constructor for the blocks.
     * -Parameters:  double x, double y, double width, double height, Colour colour, int rowID, int colID
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Brick(double x, double y, double width, double height, Colour colour, int rowID, int colID)
    {
        // Setup local data members.
        this.x = x;
        this.y = y;
        this.height = height;
        this.width = width;
        this.m_Colour = colour;
        this.k_RowID = rowID;
        this.k_ColID = colID;
    }



    /* getColour:
     *  Returns the bricks colour.
     * -Parameters:  n/a
     * -Returns:     Color
     * -Throws:      n/a
     */
    public Color getColour()
    {
        return m_Colour.getColour();
    }



    /* getValue:
     *  Returns the bricks value.
     * -Parameters:  n/a
     * -Returns:     int
     * -Throws:      n/a
     */
    public int getValue()
    {
        return m_Colour.getValue();
    }



    //-- Data Member(s):
    private Colour m_Colour = null;             // Brick colour.
    public final int k_RowID;                   // Row ID of brick in wall.
    public final int k_ColID;                   // Col ID of brick in wall.



    //-- Enum(S) & Namespace(s):
    public enum Colour                          // Brick colours.
    {
        LIGHT_PURPLE(40, new Color(202, 126, 141)),
        LIGHT_ORANGE(35, new Color(240, 163, 94)),
        LIGHT_PINK  (30, new Color(248, 202, 157)),
        LIGHT_BLUE  (25, new Color(142, 201, 187)),
        LIGHT_GREEN (20, new Color(197, 215, 192)),
        LIGHT_SALMON(15, new Color(251, 142, 126)),
        LIGHT_YELLOW(10, new Color(242, 207, 89)),
        SALMON      (5,  new Color(250, 110, 79, 255));

        //-- Constructor
        private Colour  (int value, Color colour)
        {
            k_Value = value;
            k_Colour = colour;
        }

        //-- Method(s):
        public int getValue() { return k_Value; }
        public Color getColour() { return k_Colour; }


        //-- Data Member(s:
        private final int k_Value;
        private final Color k_Colour;
    }
}
