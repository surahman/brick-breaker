            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Ball.java                       #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                    Ball Class.                    #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.geom.Ellipse2D;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Ball extends Ellipse2D.Double {

    /* Ball:
     *  Constructor for the ball.
     * -Parameters:  double x, double y, int diameter
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Ball(double x, double y, int diameter, Velocity velocity)
    {
        // Setup location.
        this.x = x;
        this.y = y;

        // Set the ellipse to be a circle.
        this.height = this.width = diameter;

        // Setup ball colour.
        m_Colour = Color.RED;

        // Set initial velocity.
        m_Velocity = velocity;
    }



    /* setVelocity:
     *  Set velocity for ball.
     * -Parameters:  Velocity velocity
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setVelocity(Velocity velocity)
    {
        m_Velocity = velocity;
    }



    /* setDiameter:
     *  Set diameter of ball.
     * -Parameters:  int diameter
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setDiameter(int diameter)
    {
        this.height = this.width = diameter;
    }



    /* setPosition:
     *  Set the position of the ball.
     * -Parameters:  double x, double y
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void setPosition(double x, double y)
    {
        this.x = x;
        this.y = y;
    }



    /* Move:
     *  Move the ball using deltas set..
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void Move()
    {
        this.x += this.deltaX * (getVelocity() / 100);
        this.y += this.deltaY * (getVelocity() / 100);
    }


    /* getColour:
     *  Return colour of ball.
     * -Parameters:  n/a
     * -Returns:     Color
     * -Throws:      n/a
     */
    public Color getColour()
    {
        return m_Colour;
    }



    /* getVelocity:
     *  Get velocity of ball.
     * -Parameters:  n/a
     * -Returns:     int
     * -Throws:      n/a
     */
    public int getVelocity()
    {
        return m_Velocity.getVelocity();
    }



    /* reset:
     *  Reset ball position to center of screen.
     * -Parameters:  n/a
     * -Returns:     int
     * -Throws:      n/a
     */
    public void reset(double winWidth, double winHeight)
    {
        // Reset motion deltas.
        this.deltaX = 1;
        this.deltaY = 1;

        this.x = winWidth / 2;
        this.y = winHeight * 0.6;

    }





    //-- Data Member(s):
    private Color m_Colour;                     // Ball colour.
    private Velocity m_Velocity;                // Velocity of ball.
    public int deltaX = 1;                      // Change in balls X-Coordinate direction.
    public int deltaY = 1;                      // Change in balls Y-Coordinate direction.



    //-- Enum(S) & Namespace(s):
    public enum Velocity                        // Ball velocities.
    {
        Slow        (200),                      // 200 px/sec
        Normal      (300),                      // 300 px/sec
        Fast        (400),                      // 400 px/sec
        Warpspeed   (800),                      // 800 px/sec
        ;                                       // List terminator.

        //-- Constructor
        private Velocity (int velocity)
        {
            k_Velocity = velocity;
        }

        //-- Method(s):
        public int getVelocity()
        {
            return k_Velocity;
        }

        //-- Data Member(s:
        private final int k_Velocity;
    }
}
