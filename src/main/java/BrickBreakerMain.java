            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    BrickBreakerMain.java           #
            # Dependencies :    LIB:                            #
            #                   USR: BrickBreaker.java          #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #            Brick Breaker Entry Point.             #
            #__________________________________________________*/



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class BrickBreakerMain {

    public static void main(String[] args) {


        try {

            // Validate argument count.
            if (args.length > 3 )
            {
                throw new IllegalArgumentException("Usage: Illegal argument count, please see documentation.");
            }


            //-- Variable Declaration(s):
            final int defaultBallSpeed = 10;                // Default ball speed.
            final int defaultFPS = 60;                      // Default frame rate in Hz.
            int ballSpeed = defaultBallSpeed;               // Ball speed.
            int FPS = defaultFPS;                           // Framerate in Hz.
            Ball.Velocity velocity = Ball.Velocity.Normal;  // Velocity to set ball too.


            // Validate and setup custom window dimensions.
            if (args.length == 2) {

                // Convert parameters to integers and validate.
                FPS = Integer.parseInt(args[0]);
                ballSpeed = Integer.parseInt(args[1]);

                if (ballSpeed < 0 || ballSpeed > 3 || FPS < 24 || FPS > 240) {
                    throw new IllegalArgumentException("Invalid arguments passed.");
                }

                // Set ball velocity.
                velocity = Ball.Velocity.values()[ballSpeed];
            }


            // Launch Breakout!
            BrickBreaker application = new BrickBreaker(FPS, velocity);

        }
        catch (Exception e)
        {
            System.err.println(e.getMessage());
            //e.printStackTrace();
        }
    }
}
