            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Viewport.java                   #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #                 Viewport Class.                   #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Viewport extends JPanel {

    /* Viewport:
     *  Constructor for the main application viewport.
     * -Parameters:  Backend backend, Dimension dims, Paddle paddle, JLabel helpText, ImageIcon logo
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public Viewport(Backend backend, Dimension dims, Paddle paddle, JLabel helpText, ImageIcon logo)
    {
        // Setup frame properties.
        this.setLayout(new BorderLayout());
        this.setBackground(new Color(50, 50, 50));
        this.setSize(dims);
        this.setFocusable(true);


        // Setup local reference to game backend and paddle.
        k_BackendRef = backend;
        k_PaddleRef = paddle;


        // Setup fonts and messages for display.
        k_VictoryMsg = "YOU WON!";
        k_LossMsg = "GAME OVER";
        k_PausedMsg = "PAUSED";
        k_LaunchMsg = "PRESS <SPACE BAR> TO LAUNCH";
        k_BannerFont = new Font("SANS_SERIF", Font.BOLD | Font.ITALIC, 40);
        k_VictoryColour = new Color(255, 80, 80);
        k_LossColour = new Color(153, 204, 255);
        k_PausedColour = new Color(89, 255, 141);


        // Mouse listener to move <Paddle>.
        this.addMouseMotionListener(

            new MouseMotionAdapter()
            {
                @Override
                public void mouseMoved(MouseEvent event)
                {
                    k_PaddleRef.movePaddle(event.getX(), getSize().width, false);
                }

            }
        );


        // Setup keyboard keystroke listeners.
        this.addKeyListener(
            new KeyAdapter()
            {
                @Override
                public void keyPressed(KeyEvent event)
                {
                    super.keyPressed(event);

                    int KeyCode = event.getKeyCode();
                    switch (KeyCode)
                    {
                        case KeyEvent.VK_RIGHT:

                            if (k_BackendRef.m_SysState == Backend.Status.ACTIVE)
                            {
                                k_PaddleRef.movePaddle(k_PaddleRef.x + k_PaddleRef.m_KBStepSize,
                                                        getSize().width,
                                                        true);
                            }

                            break;
                        case KeyEvent.VK_LEFT:

                            if (k_BackendRef.m_SysState == Backend.Status.ACTIVE)
                            {
                                k_PaddleRef.movePaddle(k_PaddleRef.x - k_PaddleRef.m_KBStepSize,
                                                        getSize().width,
                                                        true);
                            }

                            break;
                        case KeyEvent.VK_P:

                            // Check state of game and toggle pause if it makes sense.
                            if (k_BackendRef.m_SysState == Backend.Status.ACTIVE)
                            {
                                k_BackendRef.m_SysState = Backend.Status.PAUSE;
                            }
                            else if (k_BackendRef.m_SysState == Backend.Status.PAUSE)
                            {
                                k_BackendRef.m_SysState = Backend.Status.ACTIVE;
                            }

                            break;
                        case KeyEvent.VK_F1:

                            // Pause game.
                            if (k_BackendRef.m_SysState == Backend.Status.ACTIVE)
                            {
                                k_BackendRef.m_SysState = Backend.Status.PAUSE;
                            }

                            // Launch help dialog.
                            JOptionPane.showMessageDialog(null,
                                                          helpText,
                                                          "Brick Breaker -- Help?",
                                                          JOptionPane.PLAIN_MESSAGE,
                                                          logo);

                            // Un-Pause game.
                            if (k_BackendRef.m_SysState == Backend.Status.PAUSE)
                            {
                                k_BackendRef.m_SysState = Backend.Status.ACTIVE;
                            }

                            break;
                        case KeyEvent.VK_R:

                            k_BackendRef.reset();

                            break;
                        case KeyEvent.VK_SPACE:

                            if (k_BackendRef.m_SysState == Backend.Status.LAUNCH)
                            {
                                k_BackendRef.m_SysState = Backend.Status.ACTIVE;
                            }

                            break;
                        case KeyEvent.VK_BACK_SPACE:

                            System.err.println("CHEAT MODE: Bounce off bottom toggled.");
                            k_BackendRef.m_CheatBounce = !k_BackendRef.m_CheatBounce;

                            break;
                        case KeyEvent.VK_Z:

                            // Only apply cheat if game is active.
                            if (k_BackendRef.m_SysState == Backend.Status.ACTIVE)
                            {
                                k_PaddleRef.cheat(k_BackendRef.k_CheatDuration);
                            }

                            break;
                        case KeyEvent.VK_Q:

                            System.exit(0);

                            break;
                        default:
                            break;
                    }

                }
            }
        );

    }



    /* refresh:
     *  Allows an external object to force a redraw of the panel.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void refresh()
    {
        this.repaint();
    }



    /* paintComponent:
     *  Redraw of Viewport graphics context.
     * -Parameters:  Graphics graphics
     * -Returns:     n/a
     * -Throws:      n/a
     */
    @Override
    protected void paintComponent(Graphics graphics)
    {
        // Allow the parent panel to be repainted and thus allows background colour adjustment.
        super.paintComponent(graphics);

        // Setup <Scene> containing all 2D graphics to send to render engine.
        Graphics2D Scene2D = (Graphics2D) graphics;
        Scene2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);


        // Variable Declaration(s):
        Color current = null;                   // Current colour used to draw on screen.


        // Draw bricks.
        for (int row = 0; row < k_BackendRef.m_Bricks.size(); ++row)
        {
            for (int col = 0; col < k_BackendRef.m_Bricks.get(row).size(); ++col)
            {
                Brick brickRef = k_BackendRef.m_Bricks.get(row).get(col);
                Scene2D.setPaint(brickRef.getColour());
                Scene2D.fill3DRect((int)brickRef.x,
                                   (int)brickRef.y,
                                   (int)brickRef.width,
                                   (int)brickRef.height,
                                   true);
            }

        }


        // Draw ball.
        current = k_BackendRef.m_Ball.getColour();
        Scene2D.setPaint(current);
        Scene2D.fill(k_BackendRef.m_Ball);
        Scene2D.setPaint(current.brighter());
        Scene2D.draw(k_BackendRef.m_Ball);

        // Draw paddle.
        current = k_BackendRef.m_Paddle.getColour();
        Scene2D.setPaint(current);
        Scene2D.fill(k_BackendRef.m_Paddle);
        Scene2D.setPaint(current);
        Scene2D.draw(k_BackendRef.m_Paddle);


        // Draw score card, stats and lives.
        Backend.Stats stats = k_BackendRef.getStats();
        Scene2D.setPaint(Color.LIGHT_GRAY);
        Scene2D.drawString("SCORE: " + stats.m_Score, 10, 15);
        Scene2D.drawString("FPS: " + stats.m_FPS, 10, 30);
        //Scene2D.drawString("LIVES: " + stats.m_Lives, 10, 45);


        // Draw life bar at the bottom left.
        double leftOffset = 10;
        double lifeDiam = 10;
        double vPos = this.getHeight() - (2 * lifeDiam);
        for (int i = 0; i < stats.m_Lives; ++i)
        {
            double x = leftOffset + (i * 2 * lifeDiam);
            Scene2D.fillOval((int) x, (int) vPos, (int) lifeDiam, (int) lifeDiam);
        }


        // Display message banner as required.
        if (k_BackendRef.m_SysState != Backend.Status.ACTIVE)
        {
            // Calculate midpoints of display.
            double widthMidPt = getWidth() / 2;
            double heightMidPt = getHeight() / 2;

            // Setup fonts.
            Scene2D.setFont(k_BannerFont);
            FontMetrics metrics = graphics.getFontMetrics(k_BannerFont);

            switch (k_BackendRef.m_SysState) {
                case WON:

                    double winMidPt = metrics.stringWidth(k_VictoryMsg) / 2;
                    Scene2D.setColor(k_VictoryColour);
                    Scene2D.drawString(k_VictoryMsg, (int) (widthMidPt - winMidPt), (int) heightMidPt);

                    break;
                case LOST:

                    double lossMidPt = metrics.stringWidth(k_LossMsg) / 2;
                    Scene2D.setColor(k_LossColour);
                    Scene2D.drawString(k_LossMsg, (int) (widthMidPt - lossMidPt), (int) heightMidPt);

                    break;
                case PAUSE:

                    double pauseMidPt = metrics.stringWidth(k_PausedMsg) / 2;
                    Scene2D.setColor(k_PausedColour);
                    Scene2D.drawString(k_PausedMsg, (int) (widthMidPt - pauseMidPt), (int) heightMidPt);

                    break;
                case LAUNCH:

                    double launchMidPt = metrics.stringWidth(k_LaunchMsg) / 2;
                    Scene2D.setColor(k_PausedColour);
                    Scene2D.drawString(k_LaunchMsg, (int) (widthMidPt - launchMidPt), (int) heightMidPt);

                    break;
            }
        }
    }



    //-- Data Member(s):
    private final Backend k_BackendRef;         // Reference to game backend logic.
    private final Paddle k_PaddleRef;           // Reference to paddle object.
    private final Font k_BannerFont;            // Banner font to display win/loss messages.
    private final String k_VictoryMsg;          // Winner text message.
    private final String k_LossMsg;             // Loss text message.
    private final String k_PausedMsg;           // Paused text message.
    private final String k_LaunchMsg;           // Launch text message.
    private final Color k_VictoryColour;        // Colour for victory message.
    private final Color k_LossColour;           // Colour for loss message.
    private final Color k_PausedColour;         // Colour for paused message.
}
