            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    Backend.java                    #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #               Game Backend Class.                 #
            #__________________________________________________*/



//- Import(s): Library:
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class Backend {

    /* Backend:
     *  Constructor for the game backend.
     * -Parameters:  int FPS, Ball.Velocity velocity, Dimension GUIDims, JLabel helpText, ImageIcon logo
     * -Returns:     n/a
     * -Throws:      n/a
     */
    Backend(int FPS, Ball.Velocity velocity, Dimension GUIDims, JLabel helpText, ImageIcon logo)
    {
        // Setup data member(s):
        m_CheatPaddleAvailable = true;
        m_CheatBounce = false;
        m_SysState = Status.LAUNCH;
        k_FPS = FPS;
        k_Velocity = velocity;
        k_BrickCols = 16;                       // |-- TWEAK HERE AS REQUIRED.
        k_BrickRows = 8;                        // |
        k_BrickWallCoverage = 0.4;              // |
        m_Lives = 3;                            // |
        k_CheatDuration = 10;                   // |
        k_CheatThreshold = 1000;                // |


        // Setup timer handler.
        Systick_Handler = new Timer();


        // Setup paddle.
        int padWidth = 100;
        int padHeight = 8;
        m_Paddle = new Paddle((GUIDims.width / 2) - (padWidth / 2),
                              GUIDims.height - padWidth,
                               padWidth,
                               padHeight,
                               padHeight,
                              15);


        // Create viewport.
        k_Viewport = new Viewport(this, GUIDims, m_Paddle, helpText, logo);


        // Create ball in center of display.
        m_Ball = new Ball(k_Viewport.getWidth() / 2, k_Viewport.getHeight() * 0.6, 16, k_Velocity);


        // Setup bricks.
        initBrickWall();


        // Setup the dispatches to the handlers.
        setupDispatch();
    }



    /* setupDispatch:
     * Setup dispatch periodically fire handlers.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void setupDispatch()
    {

        // Redraw task handler dispatch.
        TimerTask redrawDispatch = new TimerTask()
        {
            @Override
            public void run() {
                k_Viewport.refresh();
            }
        };
        Systick_Handler.schedule(redrawDispatch, 0, k_MILLISEC/k_FPS);


        // Motion task handler dispatch.
        TimerTask motionDispatch = new TimerTask()
        {
            @Override
            public void run()
            {
                Motion_Handler();
            }
        };
        Systick_Handler.schedule(motionDispatch, 0, k_MILLISEC/k_Velocity.getVelocity());
    }



    /* Motion_Handler:
     * Moves the ball on the screen by 1px every (k_MILLISEC/k_Velocity) seconds.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void Motion_Handler()
    {
        // Only run if game is active.
        if (m_SysState != Status.ACTIVE)
        {
            return;
        }

        //-- Variable Declaration(s):
        boolean retval = false;                 // Handler return value.


        // Check for power-up condition and activate it.
        if (m_CheatPaddleAvailable && m_ScoreCard >= k_CheatThreshold)
        {
            m_CheatPaddleAvailable = false;
            m_Paddle.cheat(k_CheatDuration);
        }

        // Perform edge test.
        retval = Edge_Bounce_Handler();

        // Check for <Paddle> bounce.
        if (retval == false)
        {
            retval = Paddle_Bounce_Handler();
        }

        // Check for <Brick> wall contact.
        if (retval == false)
        {
            retval = Brick_Collision_Handler();
        }


        // Update position on screen.
        m_Ball.Move();
    }



    /* Edge_Bounce_Handler(:
     * Moves the ball on the screen by 1px every (k_MILLISEC/k_Velocity) seconds.
     * -Parameters:  n/a
     * -Returns:     boolean
     * -Throws:      n/a
     */
    private boolean Edge_Bounce_Handler()
    {
        //-- Variable Declaration(s):
        boolean retval = false;                 // Contact indicator return value.
        double x = m_Ball.getX();               // Current X-Coordinate of the ball.
        double y = m_Ball.getY();               // Current Y-Coordinate of the ball
        double diameter = m_Ball.width;         // Diameter of the ball.


        // Window bounds checking.
        if (x < 0 || x > (k_Viewport.getSize().width - diameter))
        {
            m_Ball.deltaX *= -1;
            retval = true;
        }

        // CHEAT MODE ENABLED, ALLOW BALL TO BOUNCE OFF BOTTOM.
        if (m_CheatBounce && (y < 0 || y > (k_Viewport.getSize().height - diameter)))
        {
            m_Ball.deltaY *= -1;
            return true;
        }


        // Handle game lives.
        if (y < 0)
        {
            m_Ball.deltaY *= -1;
            retval = true;
        }

        // Handle lives.
        if (y > (k_Viewport.getSize().height - diameter))
        {
            // Game over check.
            if (m_Lives == 0)
            {
                m_SysState = Status.LOST;
            }
            // Pause the system and reset ball.
            else
            {
                m_SysState = Status.LAUNCH;
                m_Ball.reset(k_Viewport.getWidth(), k_Viewport.getHeight());
                --m_Lives;
            }

            retval = true;
        }

        return retval;
    }



    /* Paddle_Bounce_Handler:
     *  Tests for collision between the <Ball> and <Paddle>.
     * -Parameters:  n/a
     * -Returns:     boolean
     * -Throws:      n/a
     */
    private boolean Paddle_Bounce_Handler()
    {

        //-- Variable Declaration(s):
        boolean retval = false;                 // Contact indicator return value.
        double x = m_Ball.getX();               // Current X-Coordinate of the ball.
        double y = m_Ball.getY();               // Current Y-Coordinate of the ball
        double diameter = m_Ball.width;         // Diameter of the ball.


        if (y >= (m_Paddle.y - diameter) && m_Ball.intersects(m_Paddle.getBounds()))
        {
            // Change <Ball> direction.
            if (m_Ball.x > m_Paddle.x && (m_Ball.x + diameter) < (m_Paddle.x + m_Paddle.width))
            {
                m_Ball.deltaY *= -1;
            }
            else
            {
                m_Ball.deltaX *= -1;
            }

            // Increment the score by 1pt for each <Paddle> strike.
            m_ScoreCard += 1;

            retval = true;
        }


        return retval;
    }



    /* Brick_Collision_Handler:
     *  Tests for collision between the <Ball> and the <Brick> wall.
     * -Parameters:  n/a
     * -Returns:     boolean
     * -Throws:      n/a
     */
    private boolean Brick_Collision_Handler()
    {

        // Check if game is won.
        if (m_Bricks.isEmpty())
        {
            m_SysState = Status.WON;
            return true;
        }


        //-- Variable Declaration(s):
        boolean retval = false;                 // Contact indicator return value.
        double x = m_Ball.getX();               // Current X-Coordinate of the ball.
        double y = m_Ball.getY();               // Current Y-Coordinate of the ball
        double diameter = m_Ball.width;         // Diameter of the ball.


        // Early exit if not in hot zone or if <Brick> wall is empty.
        if (m_Bricks.isEmpty() || (y >= m_Bricks.get(m_Bricks.size() - 1).get(0).y + m_BrickHeight))
        {
            return retval;
        }



        // Loop over rows.
        for (int rowIdx = m_Bricks.size() - 1; rowIdx >= 0; --rowIdx)
        {

            // Loop over cols.
            for (int colIdx = m_Bricks.get(rowIdx).size() - 1; colIdx >= 0; --colIdx)
            {
                Brick brickRef = m_Bricks.get(rowIdx).get(colIdx);

                // Check for collision.
                if (m_Ball.intersects(brickRef))
                {
                    retval = true;

                    // Update score card.
                    m_ScoreCard += brickRef.getValue();

                    // Delete the <Brick>.
                    m_Bricks.get(rowIdx).remove(colIdx);

                    // Check to see if the row is empty.
                    if (m_Bricks.get(rowIdx).isEmpty())
                    {
                        m_Bricks.remove(rowIdx);
                    }

                    // Change <Ball> direction.
                    if (m_Ball.x > brickRef.x && (m_Ball.x + diameter) < (brickRef.x + m_BrickWidth))
                    {
                        m_Ball.deltaY *= -1;
                    }
                    else
                    {
                        m_Ball.deltaX *= -1;
                    }

                    return retval;
                }
            }

        }


        return retval;
    }



    /* getViewport:
     *  Get reference to the viewport.
     * -Parameters:  n/a
     * -Returns:     final Viewport
     * -Throws:      n/a
     */
    public final Viewport getViewport()
    {
        return k_Viewport;
    }



    /* getStats:
     *  Gets score card and stats.
     * -Parameters:  n/a
     * -Returns:     Stats
     * -Throws:      n/a
     */
    public Stats getStats()
    {
        Stats stats = new Stats(k_FPS, m_ScoreCard, m_Lives);
        stats.m_FPS = k_FPS;

        return stats;
    }



    /* calculateBrickArea:
     *  Calculate the metrics for the brick wall.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void calculateBrickArea()
    {
        // Set offset from top of display.
        m_TopAreaHeight = 0.1 * k_Viewport.getHeight();


        // Update brick metrics.
        m_BrickHeight = (k_BrickWallCoverage * k_Viewport.getHeight()) / k_BrickRows;
        m_BrickWidth = k_Viewport.getWidth() / (k_BrickCols + 4);
    }



    /* getStats:
     *  Gets score card and stats.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void initBrickWall()
    {
        // Get the metrics for the brick wall setup.
        calculateBrickArea();

        // Initialize the offset for the y-coordinate.
        double verticalOffset = m_TopAreaHeight;


        // Initialize the brick wall.
        m_Bricks = new ArrayList<ArrayList<Brick>>(k_BrickRows);


        // Colour swatch to select colours
        Brick.Colour[] colourSwatch = Brick.Colour.values();


        // Generate <Brick> wall.
        for (int row = 0; row < k_BrickRows; ++row)
        {
            // Setup the offset for the x-coordinate.
            double leftOffset = 2 * m_BrickWidth;

            // Initialize the row.
            m_Bricks.add(new ArrayList<Brick>(k_BrickCols));

            for(int col = 0; col < k_BrickCols; ++col)
            {
                m_Bricks.get(row).add(new Brick(leftOffset,
                                                verticalOffset,
                                                m_BrickWidth,
                                                m_BrickHeight,
                                                colourSwatch[row],
                                                row,
                                                col));
                leftOffset += m_BrickWidth;
            }


            // Update the vertical offset.
            verticalOffset += m_BrickHeight;
        }
    }



    /* rescaleBricks:
     *  Rescale the brick wall.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void rescaleBricks()
    {
        // Safety Check(s):
        if (m_Bricks == null)
        {
            return;
        }

        // Update the metrics for the brick wall.
        calculateBrickArea();

        // Initialize the offset for the x-coordinate and y-coordinate.
        double leftOffset = 2 * m_BrickWidth;
        double verticalOffset = m_TopAreaHeight;

        // Resize the wall.
        for (int row = 0; row < m_Bricks.size(); ++row)
        {

            // Update positions of bricks
            for (int col = 0; col < m_Bricks.get(row).size(); ++col)
            {
                Brick brickRef = m_Bricks.get(row).get(col);

                // Resize and position <Brick>s.
                brickRef.x = leftOffset + (brickRef.k_ColID * m_BrickWidth);
                brickRef.y = verticalOffset + (brickRef.k_RowID * m_BrickHeight);
                brickRef.height = m_BrickHeight;
                brickRef.width = m_BrickWidth;
            }
        }
    }



    /* rescale:
     *  Rescale brick wall, and relocate the ball and paddle.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void rescale(Dimension oldDims, Dimension newDims)
    {
        //-- Variable Declaration(s):
        double widthScale = (double) newDims.width / (double) oldDims.width;
        double heightScale = (double) newDims.height / (double) oldDims.height;


        // Reposition <Ball>.
        m_Ball.setPosition(m_Ball.x * widthScale, m_Ball.y * heightScale);

        // Resize brick wall.
        rescaleBricks();


        // Set position of <Paddle>.
        m_Paddle.setHorizontalPos(m_Paddle.x * widthScale);
        m_Paddle.setVerticalPos(newDims.height);
    }



    /* reset:
     *  Reset the game to initial state.
     * -Parameters:  n/a
     * -Returns:     n/a
     * -Throws:      n/a
     */
    public void reset()
    {
        // Reset brick wall.
        m_Bricks = null;
        initBrickWall();

        // Reset the ball to the center of the screen.
        m_Ball.setPosition(k_Viewport.getWidth() / 2, k_Viewport.getHeight() * 0.6);
        m_Ball.deltaX = m_Ball.deltaY = 1;

        // Reset score card, lives and cheat modes.
        m_ScoreCard = 0;
        m_Lives = 3;
        m_CheatBounce = false;
        m_CheatPaddleAvailable = true;

        // Reset state to active.
        m_SysState = Status.LAUNCH;
    }




    //-- Data Member(s):
    public Ball m_Ball;                         // Ball.
    public Paddle m_Paddle;                     // Paddle.
    private final Ball.Velocity k_Velocity;     // Velocity of ball on screen.
    private final int k_FPS;                    // Frames/sec redraw rate.
    private final int k_MILLISEC = 1000;        // Milliseconds counter.
    private final Timer Systick_Handler;        // Timer to call dispatch routines.
    private final Viewport k_Viewport;          // Display viewport.
    private int m_ScoreCard = 0;                // Game score card.
    private int m_Lives = 0;                    // Player lives.
    public ArrayList<ArrayList<Brick>> m_Bricks;// Brick wall on display.
    private double m_TopAreaHeight = 0;         // Height of the top section of the screen where the score card is.
    private double m_BrickHeight = 0;           // Height of a single <Brick>.
    private double m_BrickWidth = 0;            // Width of a single <Brick>.
    private final int k_BrickCols;              // Number of <Brick> columns.
    private final int k_BrickRows;              // Number of <Brick> rows.
    private final double k_BrickWallCoverage;   // Percentage vertical of screen coverage given to brick wall.
    public Status m_SysState;                   // Current system activity state.
    private final int k_CheatThreshold;         // Threshold to trigger <Paddle> power-up.
    public final int k_CheatDuration;           // Duration in seconds to keep <Paddle> power-up active.
    public boolean m_CheatBounce;               // Allow the <Ball> to bounce off bottom.
    public boolean m_CheatPaddleAvailable;      // Paddle upgrade availability cheat available once a game.



    //-- Class Definition(s):
    public class Stats                          // Tuple, because Java is a turd that doesn't support this.
    {
        //-- Constructor(s):
        public Stats(int FPS, int score, int lives)
        {
            m_FPS = FPS;
            m_Score = score;
            m_Lives = lives;
        }

        //-- Data Member(s):
        public int m_Score;
        public int m_FPS;
        public int m_Lives;
    }



    //-- Enum(S) & Namespace(s):
    public enum Status                          // Game state.
    {
        ACTIVE,                                 // Active game.
        PAUSE,                                  // Paused game.
        WON,                                    // Won game.
        LOST,                                   // Lost game.
        LAUNCH,                                 // Launch ball mode.
        ;                                       // List terminator.
    }
}
