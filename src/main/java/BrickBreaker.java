            /*__________________________________________________#
            #  Life is a journey, and it's the experiences we   #
            #  have along the way that define us. To shy from   #
            #  the difficult path least traveled is a travesty, #
            #  for it's here that we find the greatest bounty.  #
            #                                                   #
            #               ~ Perpetual Form ~                  #
            #                                                   #
            #   For my father, who taught me the true meaning   #
            #                   of courage.                     #
            #___________________________________________________#
            # File Name    :    BrickBreaker.java               #
            # Dependencies :    LIB:                            #
            #                   USR: n/a                        #
            # Toolchain    :    JDK 1.8, Gradle 5.4             #
            # Compilation  :    gradle build                    #
            #___________________________________________________#
            #    https://www.linkedin.com/in/saad-ur-rahman/    #
            #  Copyright Saad Ur Rahman, All rights reserved.   #
            #     Usage and source code is subject to the       #
            #           GNU Affero GPL v3.0 license.            #
            #___________________________________________________#
            #                                                   #
            #              Brick Breaker Class.                 #
            #__________________________________________________*/



//- Import(s): Library:
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.*;



            /*_________________________________________________ #
            #               Object Definitions                  #
            # _________________________________________________*/



public class BrickBreaker {

    /* BrickBreaker:
     *  Constructor for the main application window.
     * -Parameters:  int FPS, int ballSpeed
     * -Returns:     n/a
     * -Throws:      n/a
     */
    BrickBreaker(int FPS, Ball.Velocity velocity)
    {
        // Setup resource directories and window sizes. TWEAK HERE AS NEEDED.
        k_AppIconsDir = "/app/";
        Dimension GUIDims = new Dimension(1280, 720);
        k_FPS = FPS;
        k_BallVelocity = velocity;


        // Setup Icon.
        k_AppIcons = LoadAppIcon(k_AppIconsDir);


        // Create new GUI thread to be launched by event-dispatcher thread.
        SwingUtilities.invokeLater (
            new Runnable()
            {
                public void run() {
                    DrawMainWindow(GUIDims);
                }
            }
        );
    }



    /* DrawMainWindow:
     *  Draw the main GUI window.
     * -Parameters:  Dimension GUIDims
     * -Returns:     n/a
     * -Throws:      n/a
     */
    private void DrawMainWindow(Dimension GUIDims)
    {
        // Display boot splash screen.
        SplashScreen();


        // Setup GUI window.
        m_GUI = new MainWindow(GUIDims);

    }



    /* SplashScreen:
     *  Deploy the splash screen dialog.
     * -Parameters:  n/a
     * -Returns:     void
     * -Throws:      n/a
     */
    private void SplashScreen()
    {
        m_SplashLogo = LoadImageIcon ("/ball/", "ball_128px.png");

        // Text area for dialog box.
        m_HelpScreenTxt = new JLabel (
                "<html>" +
                "<h1 style=\"text-align: center;\"><span style=\"color: #ff6600;\">" +
                "<em><strong>Brick Breaker</strong></em></span></h1>" +
                "<p style=\"text-align: left;\"><span style=\"text-decoration: underline;\">Usage:</span></p>" +
                "<ul>" +
                "<li style=\"text-align: left;\">Press F1 to bring up this help window and pause game.</li>" +
                "<li style=\"text-align: left;\">Press p to pause the game.</li>" +
                "<li style=\"text-align: left;\">Use the Left and Right arrow keys to move the paddle.</li>" +
                "<li style=\"text-align: left;\">Drag the mouse to move the paddle.</li>" +
                "<li style=\"text-align: left;\">Press the Space Bar to launch the ball.</li>" +
                "<li style=\"text-align: left;\">Press r to reset the game.</li>" +
                "<li style=\"text-align: left;\">Press z to enable the paddle power-up for 10 seconds.</li>" +
                "<li style=\"text-align: left;\">Press Back Space to enable bouncing off the bottom of the screen." +
                "</li>" +
                "<li style=\"text-align: left;\">Press q to quit the game.</li>" +
                "</ul>" +
                "<p style=\"text-align: left;\"><span style=\"text-decoration: underline;\">Objectives:</span></p>" +
                "<ul>" +
                "<li>Learn how to build an interactive application using Java 8 and Swing.</li>" +
                "<li>Learn how to handle real-time drawing and animation of simple graphics.</li>" +
                "<li>Utilize model-view-controller to structure your application.</li>" +
                "</ul>" +
                "<div> <br/><br/>" +
                "<p style=\"text-align: center;\"><em>&copy; Saad Ur Rahman. All rights reserved.</em></p>" +
                "</div>" +
                "<div>" +
                "<p style=\"text-align: center;\"><em><sub>Usage and source code is licensed under the GNU " +
                "Affero General Public License v3.0.</sub></em></p>" +
                "<p style=\"text-align: center;\"><em><sub>Icons made by www.Freepik.com from" +
                "www.FlatIcon.com are licensed by Creative Commons BY 3.0.</sub></em></p>" +
                "</div>" +
                "</html>"
        );


        // Launch splash screen to welcome users.
        JOptionPane.showMessageDialog(m_GUI,
                                      m_HelpScreenTxt,
                                      "Brick Breaker",
                                      JOptionPane.PLAIN_MESSAGE,
                                      m_SplashLogo);
    }



    /* LoadImageIcon:
     *  Load an <ImageIcon> from the <resources> subdirectory.
     * -Parameters:  final String baseDir, final String iconName
     * -Returns:     ImageIcon
     * -Throws:      n/a
     */
    private ImageIcon LoadImageIcon (final String baseDir, final String iconName)
    {
        //-- Variable Declaration(s):
        InputStream istream = null;             // Input stream for icon image.
        ImageIcon icon = null;                  // Loaded <icon> image for button.

        try
        {
            // Setup <icon>.
            istream = getClass().getResourceAsStream(baseDir + iconName);
            if (istream == null) { throw new IOException(); }

            icon = new ImageIcon(ImageIO.read(istream));
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Failed to load " + iconName + " : " + e.toString());
        }

        return icon;
    }



    /* LoadAppIcon:
     *  Load all the application <BufferedImage>s from the <resources> subdirectory.
     * -Parameters:  final String baseDir
     * -Returns:     List<BufferedImage>
     * -Throws:      n/a
     */
    private List<BufferedImage> LoadAppIcon (final String baseDir)
    {
        //-- Variable Declaration(s):
        InputStream istream = null;             // Input stream for icon image.
        List<BufferedImage> appIcons = null;    // <ArrayList> containing application icons.

        try
        {
            // Read all file names from resource directory.
            istream = getClass().getResourceAsStream(baseDir);
            if (istream == null) { throw new IOException(); }

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(istream));

            // Load icons into <ArrayList>.
            String filename;
            appIcons = new ArrayList<>();
            while ( ( filename = bufferedReader.readLine() ) != null)
            {
                InputStream imageStream = getClass().getResourceAsStream(baseDir + filename);
                if (imageStream == null) { throw new IOException(); }

                appIcons.add(ImageIO.read(imageStream));
            }
        }
        catch (IOException e)
        {
            System.out.println("ERROR: Failed to load application icons in " + baseDir + " : " + e.toString());
        }

        return appIcons;
    }



    //-- Data Member(s):
    private final String k_AppIconsDir;         // Main GUI icons directory.
    private final List<BufferedImage> k_AppIcons;// Application icons for GUI windows.
    private ImageIcon m_SplashLogo;             // Splash screen logo.
    private JFrame m_GUI = null;                // Main GUI window.
    private final int k_FPS;                    // Frames per second redraw rate.
    private final Ball.Velocity k_BallVelocity; // Rate of motion of the ball.
    public JLabel m_HelpScreenTxt;              // Static help screen text.



    // Main JFrame window to control aspect ratio of resize.
    private class MainWindow extends JFrame
    {

        /* MainWindow:
         *  Constructor for the main application window to support aspect ratio locking.
         * -Parameters:  Dimension GUIDims
         * -Returns:     n/a
         * -Throws:      n/a
         */
        MainWindow(Dimension GUIDims)
        {
            // Setup GUI parameters.
            this.setTitle("Brick Breaker");
            this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            this.setMinimumSize(GUIDims);
            this.setSize(GUIDims);
            this.setVisible(true);
            this.setIconImages(k_AppIcons);
            this.pack();
            this.setLocationRelativeTo(null);
            Dimension currentDims = new Dimension(getWidth(), getHeight());

            // Initialize game backend.
            Backend backend = new Backend(k_FPS, k_BallVelocity, GUIDims, m_HelpScreenTxt, m_SplashLogo);


            // Setup viewport panel.
            this.add(backend.getViewport());


            // Setup component listeners.
            this.addComponentListener(
                new ComponentAdapter()
                {

                    @Override
                    public void componentResized(ComponentEvent event) {
                        super.componentResized(event);

                        // Store new window dimensions.
                        Dimension oldDims = new Dimension(currentDims);
                        currentDims.width = getWidth();
                        currentDims.height = getHeight();


                        // Resize <Brick> wall and reposition <Ball> and <Paddle>.
                        backend.rescale(oldDims, currentDims);

                    }
                }
            );

        }

    }

}
