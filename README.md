
<p align="center" width="100%">
    <img src="./src/main/resources/app/brick-breaker_256px.png">
</p>

## Brick Breaker

[ License ] |
:---------: |

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `LICENSE` in the root directory of this project and can also be found online
<a href="https://opensource.org/licenses/AGPL-3.0" title="GNU Affero General Public License v3.0">here</a>.


<br>
<br>

[ System Specs ] |
:---------: |

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the toolsets shouldn't break the build.

|Software| Version
|:------------: | :-------------:|
Java Runtime Environment | build 1.8.0_212-b10 x64
Java Development Kit | build 1.8.0_212 x64
Gradle | 5.4.1
Windows 10 Pro | 1903 x64

<br>
<br>

|[ Usage & Build ] |
|:---------: |

[![pipeline status](https://gitlab.com/surahman/brick-breaker/badges/main/pipeline.svg)](https://gitlab.com/surahman/brick-breaker/-/commits/main)

- [X] Executing `gradle build` will build *Brick Breaker*.
- [x] Executing `gradle run --args='FPS Ball_Speed'` will run *Brick Breaker*. Note that is not necessary to run the
`build` command above; you can simply execute the `run` command to build and start the application.
- [x] Included in the root directory of this project is the archive `Brick Breaker-1.1.zip` which has a self-contained
fat jar file. To run the application simply extract the contents of the archive, open a command prompt and navigate to
where you extracted the files. Running the command `java -jar "Brick Breaker-1.1.jar"` will launch the application.

<br>
<br>

|[ Architecture ] |
|:---------:|
##### Design ![Design](./src/main/resources/app/brick-breaker_24px.png)
The design and division of the components as per the _MVC_ model is as follows.

###### Model ![Model](./src/main/resources/app/brick-breaker_16px.png)
The _Model_ is comprised of the _Backend, Ball, Brick,_ and _Paddle_ classes.
* **Backend**: Contains all the game dispatch logic for timers as well as collision, setup and event handling.
* **Ball**: Contains the class that describes all the properties for the onscreen _Ball_.
* **Brick**: Contains the class that describes all the properties for the _Bricks_. Each of the _Brick_ colours and
points associated with the colour are stored within the _Brick_ class.
* **Paddle**: Contains the class the describes all the properties for the onscreen _Paddle_. It also contains the
timer thread definition that controls the 10 second power-up.

###### View ![View](./src/main/resources/app/brick-breaker_16px.png)
The _View_ is comprised of the _Viewport_ class.
* **Stats**: Displays all the game statistics on the screen (Score, FPS and Lives).
* **Bricks**: Draws the _Brick_ wall on the screen by iterating over the array of _Bricks_ in the _Backend_.
* **Ball**: Draws the _Ball_ on the screen, movement is handled by the _Backend_.
* **Paddle**: Draws the _Paddle_ on the screen, movement is handled by the _Backend_.
* **Messages**: Draws messages on the screen based on the status of the _Backend_.

###### Controller ![Controller](./src/main/resources/app/brick-breaker_16px.png)
The _Controller_ is comprised of the _Viewport_ class.
* **Paddle**: Registers movement of the _mouse_ as well as _left_ and _right_ arrow key presses to move
the _Paddle_.
* **Pause**: Listens for the _p_ key to pause the game.
* **Launch Ball**: Listens for the _Space Bar_ key to launch the ball when the player is ready.
* **Reset**: Listens for the _r_ key to reset the game.
* **Help**: Listens for the _F1_ key to bring up the help screen.
* **Quit**: Listens for the _q_ key to quit the game.
* **Power-up**: Listens for the _z_ key to enable the _Paddle_ power-up for 10 seconds.
* **Solid Bottom**: Listens for the _Back Space_ key to enable bouncing off the bottom of the window.

<br>
<br>

|[ Features ] |
|:---------:|


###### Screen Shots:
Screenshots are located <a href="screenshots/" title="screenshots">here</a>.
<p align="center" width="100%">
    <img src="./screenshots/screenshot_003.png" width="50%" height="50%">
</p>


##### Parameters ![Parameters](./src/main/resources/app/brick-breaker_24px.png)

###### Frames per Second ![FPS](./src/main/resources/ball/ball_16px.png)
- [x] FPS: Controls the frames drawn per second, which is capped in the range of 24-240 FPS, inclusive.
- [x] Default: If no parameters are provided the _FPS_ rate is set to 60 to match screen refresh rates.

###### Ball Speed ![Ball Speed](./src/main/resources/ball/ball_16px.png)
The speed of the _Ball_ can be set to any of the speeds listed below:
- [x] 0 - Slow: This is 1 pixel per **1000/200** seconds.
- [x] 1 - Normal: This is 1 pixel per **1000/300** seconds.
- [x] 2 - Fast: This is 1 pixel per **1000/400** seconds.
- [x] 3 - Warpspeed: This is 1 pixel per **1000/800** seconds.
- [x] Default: If no parameters are provided the default _Ball_ speed is set to _Normal_.

<br/>

##### Splash Screen ![Splash Screen](./src/main/resources/app/brick-breaker_24px.png)
- [x] Splash Screen: Before the application boots up a splash screen is displayed that contains usage information.
- [x] Help Screen: Pressing _F1_ during game play will pause the game and bring up the help screen dialog.

<br/>

##### Main Features ![Main Features](./src/main/resources/app/brick-breaker_24px.png)
- [x] At least 5 rows of _Bricks_ along the top of the screen. There are actually 8 row which conform to the colour code
in the picture on the assignment specification page.
- [x] _Paddle_ along the bottom that can be moved with keys and mouse.
- [x] _Ball_ moves across the screen, bouncing off the side and top walls in a realistic fashion.
- [x] _Ball_ hits a _Brick_, the _Brick_ disappears.
- [x] _Ball_ strikes the _Paddle_, it bounces.
- [x] _Ball_ touches the bottom of the screen, the game ends.
- [x] Keep track of the players score, awarding points for every bounce of the _Ball_. Display the current score
on-screen during the game.
- [x] Game should play smoothly and animate/repaint in a way that supports smooth animation.
- [x] Game must respond to window resize events; _Brick_ are resized and _Ball_ and _Paddle_ are repositioned.

<br/>

##### Addition Features ![Additional Features](./src/main/resources/app/brick-breaker_24px.png)
- [x] Power-up: Once the score reaches 1000 points the _Paddle_ size is upgraded and the colour switched to orange for
10 seconds.
- [x] Bottom Wall: A feature where the bottom of the screen can be turned into a wall allowing the ball to bounce off
the bottom.
- [x] Lives: **3** extra lives are allotted to each player and are displayed in the bottom left of the screen.
- [x] Pause: Game can be paused by pressing a key.
- [x] Reset: Ability to reset the game.

<br/>

##### Key Mappings ![Key Mappings](./src/main/resources/app/brick-breaker_24px.png)
* **F1**: Brings up the help screen with all the key mappings.
* **p**: Toggles the pause function for the game.
* **Left | Right**: Arrow keys move the _Paddle_ on the screen.
* **Mouse Motion**: Moving the mouse moves the _Paddle_ on the screen.
* **Space Bar**: Launches the _Ball_ and makes the game active.
* **r**: Resets the game to its starting state.
* **z**: Enables the _Paddle_ power-up for 10 seconds as a cheat.
* **Backspace**: Turns the bottom of the screen into a wall that the _Ball_ can bounce off.
* **q**: Quits the game and closes the window.


<br/>

##### Onscreen Information: ![Onscreen](./src/main/resources/app/brick-breaker_24px.png)

###### Score ![Score](./src/main/resources/app/brick-breaker_16px.png)
Each bounce off the _Paddle_ is worth 1 point whilst each _Brick_ has its own score, as outline below. The score is
displayed on the top left-hand side of the screen.
* LIGHT PURPLE: 40 points.
* LIGHT ORANGE: 35 points.
* LIGHT PINK: 30 points.
* LIGHT BLUE: 25 points.
* LIGHT GREEN: 20 points.
* LIGHT SALMON: 15 points.
* LIGHT YELLOW: 10 points.
* SALMON: 5 points.

###### FPS ![Score](./src/main/resources/app/brick-breaker_16px.png)
The currently active frames per second is displayed in the top left of the screen, just below the score.

###### Lives ![Score](./src/main/resources/app/brick-breaker_16px.png)
The players lives are visible on the bottom left-hand side of the screen. Each of the circles represents the number of
_Balls_ the player has available yet to play

<br/>
<br/>


---
#### *__Development Log__*

---
<br/>

Projects were initially uploaded to a private development repository where it was developed using the Git Flow paradigm.
This repository was created to make this project public and package it in a `fat jar` file.

<br/>


##### [ June 15th 2019 ]
> * Initial project workspace setup.
> * Main window created with icons and background colour.
> * Splash screen and help window created and setup to open on launch.

<br/>


##### [ June 18th 2019 ]
> * Experimenting with extending the _Ellipse2D_ class to support drawing functionality.
> * Leaving the _Ball_ as a simple _Ellipse2D.Double_ object for now.
> * Working on _Viewport_ object as display for moving objects and scorecard.


<br/>

##### [ June 19th 2019 ]
> * Capped _FPS_ to range [24 - 240].
> * Created _Brick_ class.
> * Added enums for _Speed_ of _Ball_ and a few getter and setter functions.
> * Added game _Backend_ class which will run the mechanics and logic of the the game.


<br/>

##### [ June 20th 2019 ]
> * Made _splash screen_ show on boot.
> * Working on getting **SysTick** style timer dispatcher system up and running in _Backend_.
> * Reordered construction order: _Backend_ now bootstraps the entire game, including the _Viewport_.
> * Fixed background colour issue so that the _parent_ object is passed the request to repaint as well.
> * Bouncing _Ball_ working, but there is some motion blur associated with the physical screen refresh rates and the
    redraw rate disparity.
> * Default _FPS_ set to 60 and _Ball_ motion is independent of the frame rate.


<br/>

##### [ June 21st 2019 ]
> * Moved help screen to _Viewport_ so that key stroke handling is cleaner.
> * Moved _Ball_ delta's into _Ball_ class to make things cleaner.
> * _Paddle_ follows the mouse, need to add keyboard listener for _left_ and _right_ keys.
> * _Paddle_ position conforms to screen resizing.
> * ~~Working on aspect ratio locking.~~ Forget about aspect and custom icons - not required.


<br/>

##### [ June 22nd 2019 ]
> * Created adaptive _Brick_ section that conforms to rescaling.
> * Created _Colour_ space for _Brick_'s with self contained values and colours.
> * Setup _Brick_ class to use _Colour_ and wall generator to select colour based on row ID.
> * _Score Card_ setup and functioning.
> * Completed _Ball_ and _Paddle_ collision logic.



<br/>

##### [ June 23rd 2019 ]
> * ~~Bug with collision detection with edges of edges with _Ball_. Solution is to change **X Coordinate** direction
    on collisions with sides.~~ Bug fixed with both _Paddle_ and _Brick_ wall classes.
> * Completed collider for _Brick_ wall.
> * Added lives bar on bottom left of screen.



<br/>

##### [ June 23rd 2019 ]
> * Added controls to _Pause_ and _Reset_ game.
> * Repositioning _Ball_ on rescale.
> * Repositioning _Paddle_ on rescale.
> * Added game status message to output for _Victory, Loss_ and _Paused_ states.
> * Fixed bug in _Reset_ to ensure _Ball_ direction is reset to fall.



<br/>

##### [ June 24th 2019 ]
> * Created _Cheat_ mode to increase size of _Paddle_ for 10secs. Need to create white power up block.
> * Bug fixes in various _Pause_ mode issues.



<br/>

##### [ June 25th 2019 ]
> * Bug fixes in adjusting _Paddle_ position.
> * _Ball_ falls off screen but cheat mode to turn it off.
> * _Ball_ bounces off _Paddle_ now earn 1pt per bounce.
> * Created a _Launch_ mode.
> * _Paddle_ power-up now sets colour of _Paddle_ to orange.
> * Completed cheat mode to allow _Ball_ to bounce off bottom of the screen.
> * Parameter handling in main function to set _Ball_ speed and _FPS_.
> * Created cheat mode to activate once when score passes 1000 points.
> * Added _q_ to quit option.
> * Completed help dialog text.
> * Completed documentation for submission.



<br/>

##### [ June 28th 2019 ]
> * Reduced edge detection accuracy to improve interaction with _Paddle_ and _Bricks_.



<br/>

##### [ October 8th 2020 ]
> * Began work prepping resource access to files to support fat jar creation.
> * Created and moved icons and images to `/src/main/resources/` directory to support packaging in fat jar.
> * Ported loading of icons and resources to support fat jar loading.


<br/>

---

###### Copyright and Legal Notice(s):
&copy; Saad Ur Rahman. All rights reserved. Usage and source code is licensed under the
`GNU Affero General Public License v3.0`.

<div>Icons made by <a href="https://www.freepik.com/" title="Freepik">Freepik</a> from
<a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"
title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a>.</div>

---
