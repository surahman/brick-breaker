

    ,---,.                               ,-.             ,---,.                                 ,-.
  ,'  .'  \         ,--,             ,--/ /|           ,'  .'  \                            ,--/ /|
,---.' .' | __  ,-,--.'|           ,--. :/ |         ,---.' .' | __  ,-.                  ,--. :/ |           __  ,-.
|   |  |: ,' ,'/ /|  |,            :  : ' /          |   |  |: ,' ,'/ /|                  :  : ' /          ,' ,'/ /|
:   :  :  '  | |' `--'_      ,---. |  '  /           :   :  :  '  | |' |,---.    ,--.--.  |  '  /     ,---. '  | |' |
:   |    ;|  |   ,,' ,'|    /     \'  |  :           :   |    ;|  |   ,/     \  /       \ '  |  :    /     \|  |   ,'
|   :     '  :  / '  | |   /    / '|  |   \          |   :     '  :  //    /  |.--.  .-. ||  |   \  /    /  '  :  /
|   |   . |  | '  |  | :  .    ' / '  : |. \         |   |   . |  | '.    ' / | \__\/: . .'  : |. \.    ' / |  | '
'   :  '; ;  : |  '  : |__'   ; :__|  | ' \ \        '   :  '; ;  : |'   ;   /| ," .--.; ||  | ' \ '   ;   /;  : |
|   |  | ;|  , ;  |  | '.''   | '.''  : |--'         |   |  | ;|  , ;'   |  / |/  /  ,.  |'  : |--''   |  / |  , ;
|   :   /  ---'   ;  :    |   :    ;  |,'            |   :   /  ---' |   :    ;  :   .'   ;  |,'   |   :    |---'
|   | ,'          |  ,   / \   \  /'--'              |   | ,'         \   \  /|  ,     .-.'--'      \   \  /
`----'             ---`-'   `----'                   `----'            `----'  `--`---'              `----'




+--------------+
|   License    |
+--------------+

This project is made available under the `GNU Affero General Public License v3.0` for the sole purpose of demonstrating
my programming and software development capabilities. Please adhere to the permissions, conditions and limitations
which are outlined in the `GNU Affero General Public License v3.0`.

The license file is included as `agpl-3.0.txt` in the root directory of this project and can also be found online here:
https://opensource.org/licenses/AGPL-3.0




+--------------+
| System Specs |
+--------------+

This was the system configuration of the local development and test environments. Whilst it is recommended to have a
similar setup, newer versions of the toolsets shouldn't break the build.

                            +--------------------------+--------------------------+
                            |         Software         |          Version         |
                            +--------------------------+--------------------------+
                            | Java Runtime Environment |  build 1.8.0_212-b10 x64 |
                            |   Java Development Kit   |    build 1.8.0_212 x64   |
                            |          Gradle          |           5.4.1          |
                            |      Windows 10 Pro      |         1903 x64         |
                            +--------------------------+--------------------------+




+---------+
|  Usage  |
+---------+

- [X] Executing `gradle build` will build *Brick Breaker*.
- [x] Executing `gradle run --args='FPS Ball_Speed'` will run *Brick Breaker*. Note that is not necessary to run the
      `build` command above; you can simply execute the `run` command to build and start the application.
- [x] Included in the root directory of this project is the archive `Brick Breaker-1.1.zip` which has a self-contained
      fat jar file. To run the application simply extract the contents of the archive, open a command prompt and
      navigate to where you extracted the files. Running the command `java -jar "Brick Breaker-1.1.jar"` will launch
      the application.


+--------------+
| Architecture |
+--------------+


##### Design
The design and division of the components as per the _MVC_ model is as follows.

## Model
The _Model_ is comprised of the _Backend, Ball, Brick,_ and _Paddle_ classes.

- [x] **Backend**: Contains all the game dispatch logic for timers as well as collision, setup and event handling.
- [x] **Ball**: Contains the class that describes all the properties for the onscreen _Ball_.
- [x] **Brick**: Contains the class that describes all the properties for the _Bricks_. Each of the _Brick_ colours and
      points associated with the colour are stored within the _Brick_ class.
- [x] **Paddle**: Contains the class the describes all the properties for the onscreen _Paddle_. It also contains the
      timer thread definition that controls the 10 second power-up.

## View
The _View_ is comprised of the _Viewport_ class.

- [x] **Stats**: Displays all the game statistics on the screen (Score, FPS and Lives).
- [x] **Bricks**: Draws the _Brick_ wall on the screen by iterating over the array of _Bricks_ in the _Backend_.
- [x] **Ball**: Draws the _Ball_ on the screen, movement is handled by the _Backend_.
- [x] **Paddle**: Draws the _Paddle_ on the screen, movement is handled by the _Backend_.
- [x] **Messages**: Draws messages on the screen based on the status of the _Backend_.

## Controller
The _Controller_ is comprised of the _Viewport_ class.

- [x] **Paddle**: Registers movement of the _mouse_ as well as _left_ and _right_ arrow key presses to move
      the _Paddle_.
- [x] **Pause**: Listens for the _p_ key to pause the game.
- [x] **Launch Ball**: Listens for the _Space Bar_ key to launch the ball when the player is ready.
- [x] **Reset**: Listens for the _r_ key to reset the game.
- [x] **Help**: Listens for the _F1_ key to bring up the help screen.
- [x] **Quit**: Listens for the _q_ key to quit the game.
- [x] **Power-up**: Listens for the _z_ key to enable the _Paddle_ power-up for 10 seconds.
- [x] **Solid Bottom**: Listens for the _Back Space_ key to enable bouncing off the bottom of the window.



+----------+
| Features |
+----------+


##### Parameters

## Frames per Second
- [x] FPS: Controls the frames drawn per second, which is capped in the range of 24-240 FPS, inclusive.
- [x] Default: If no parameters are provided the _FPS_ rate is set to 60 to match screen refresh rates.

## Ball Speed
The speed of the _Ball_ can be set to any of the speeds listed below:

- [x] 0 - Slow: This is 1 pixel per **1000/200** seconds.
- [x] 1 - Normal: This is 1 pixel per **1000/300** seconds.
- [x] 2 - Fast: This is 1 pixel per **1000/400** seconds.
- [x] 3 - Warpspeed: This is 1 pixel per **1000/800** seconds.
- [x] Default: If no parameters are provided the default _Ball_ speed is set to _Normal_.



##### Splash Screen
- [x] Splash Screen: Before the application boots up a splash screen is displayed that contains usage information.
- [x] Help Screen: Pressing _F1_ during game play will pause the game and bring up the help screen dialog.



##### Main Features
- [x] At least 5 rows of _Bricks_ along the top of the screen. There are actually 8 row which conform to the colour code
      in the picture on the assignment specification page.
- [x] _Paddle_ along the bottom that can be moved with keys and mouse.
- [x] _Ball_ moves across the screen, bouncing off the side and top walls in a realistic fashion.
- [x] _Ball_ hits a _Brick_, the _Brick_ disappears.
- [x] _Ball_ strikes the _Paddle_, it bounces.
- [x] _Ball_ touches the bottom of the screen, the game ends.
- [x] Keep track of the players score, awarding points for every bounce of the _Ball_. Display the current score
      on-screen during the game.
- [x] Game should play smoothly and animate/repaint in a way that supports smooth animation.
- [x] Game must respond to window resize events; _Brick_ are resized and _Ball_ and _Paddle_ are repositioned.



##### Addition Features
- [x] Power-up: Once the score reaches 1000 points the _Paddle_ size is upgraded and the colour switched to orange for
      10 seconds.
- [x] Bottom Wall: A feature where the bottom of the screen can be turned into a wall allowing the ball to bounce off
      the bottom.
- [x] Lives: **3** extra lives are allotted to each player and are displayed in the bottom left of the screen.
- [x] Pause: Game can be paused by pressing a key.
- [x] Reset: Ability to reset the game.



##### Key Mappings
- [x] **F1**: Brings up the help screen with all the key mappings.
- [x] **p**: Toggles the pause function for the game.
- [x] **Left | Right**: Arrow keys move the _Paddle_ on the screen.
- [x] **Mouse Motion**: Moving the mouse moves the _Paddle_ on the screen.
- [x] **Space Bar**: Launches the _Ball_ and makes the game active.
- [x] **r**: Resets the game to its starting state.
- [x] **z**: Enables the _Paddle_ power-up for 10 seconds as a cheat.
- [x] **Backspace**: Turns the bottom of the screen into a wall that the _Ball_ can bounce off.
- [x] **q**: Quits the game and closes the window.




##### Onscreen Information:

## Score
Each bounce off the _Paddle_ is worth 1 point whilst each _Brick_ has its own score, as outline below. The score is
displayed on the top left-hand side of the screen.

- [x] RED: 40 points.
- [x] DARK ORANGE: 35 points.
- [x] ORANGE: 30 points.
- [x] YELLOW: 25 points.
- [x] GREEN: 20 points.
- [x] BLUE: 15 points.
- [x] LIGHT PURPLE: 10 points.
- [x] PURPLE: 5 points.

## FPS
The currently active frames per second is displayed in the top left of the screen, just below the score.

## Lives
The players lives are visible on the bottom left-hand side of the screen. Each of the circles represents the number of
_Balls_ the player has available yet to play



+-------------------------------+
| Copyright and Legal Notice(s) |
+-------------------------------+

© Saad Ur Rahman. All rights reserved.

Icons made by Freepik from www.flaticon.com is licensed by CC 3.0 BY.
